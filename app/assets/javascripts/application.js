// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.purr
//= require best_in_place
//= require plugins/jquery.readySelector
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require jquery.rwdImageMaps.min
//= require ckeditor/init
//= require turbolinks
//= require_tree .

// Use this website for figuring out the image coordinates:
// https://www.image-map.net/ 

$(document).on('turbolinks:load', function() {
    // needed to add this code as well as the content for head for ckeditor to load without page refresh, dont forget to add class ckeditor to cktext_area
    $('.ckeditor').each(function() {
      CKEDITOR.replace($(this).attr('id'));
    });

        // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything or links that you dont want to scroll (bootstrap)
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href="#carousel-example-generic"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 2000, function() {
            // // THIS SECTION ADDS A FOCUS TO THE AREA SCROLLED - ALEX
            // // Callback after animation
            // // Must change focus!
            // var $target = $(target);
            // $target.focus();
            // if ($target.is(":focus")) { // Checking if the target was focused
            //   return false;
            // } else {
            //   $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            //   $target.focus(); // Set focus again
            // };
          });
        }
      }
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        autoplay: 2500,
        margin: 10,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: false
          },
          620: {
            items: 2,
            nav: false
          }, 
          992: {
            items: 3,
            nav: true,
            loop: true,
            margin: 20
          }
        }
    })

    if ($('body.home.index').length > 0) {
      $('img[usemap]').rwdImageMaps();

      $("map area").on('click', function (event) {
        event.preventDefault();
        // $('<p>Text</p>').appendTo('#mydiv');
        // $("<p>" + $(this).attr('title') + "</p>").appendTo('#mydiv');
        alert($(this).attr('title'));
      });
    }

    if ($('body.home.location_florida').length > 0) {
      $('.best_in_place').best_in_place();
    }


    // I have 3 different views using the following functions, therefore you will see the id's separated with commas
    if ($('body.blogs.edit').length > 0 || $('body.blogs.new').length > 0 || $('body.profiles.new').length || $('body.profiles.edit').length || $('body.locations.new').length || $('body.locations.edit').length) {
      $("#blog_images_attributes_0_pic,#profile_image_attributes_pic,#location_image_attributes_pic").change(function(evt) { 
        if (evt.target.files && evt.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (event) {
                $('#current_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(169);

                $('#square_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(300);

                $('#location_image')
                    .attr('src', event.target.result)
                    .width(500)
                    .height(250);
            };

            reader.readAsDataURL(evt.target.files[0]);
        }
      });
    } 
});


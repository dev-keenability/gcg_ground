class HomeController < ApplicationController
  def index
    @blogs = Blog.order(pubdate: "desc").take(3)
  end

  def services_ramp_handling
  end

  def services_aircraft_cleaning
  end

  def services_passenger_services
  end

  def services_ground_support
  end

  def services_janitorial_services
  end

  def services_cargo_handling
  end

  def services_baggage_services
  end

  def location_florida
    @profiles = Profile.where(location: 'Florida').order(position: "asc")
  end

  def location_montego_bay
    @profiles = Profile.where(location: 'Montego-Bay').order(position: "asc")
  end

  def location_kingston
    @profiles = Profile.where(location: 'Kingston').order(position: "asc")
  end

  def location_st_thomas
    @profiles = Profile.where(location: 'St-Thomas').order(position: "asc")
  end

  def about
  end
  
  def join
  end
  
  def privacy_policy
  end
  
  def management
  end

  def catch_all
    flash.now[:warning] = "The page or request you were looking for was not found."
    @blogs = Blog.order(pubdate: "desc").take(3)
    render :index
  end

end

class Image < ApplicationRecord
  
  belongs_to :imageable, polymorphic: true, optional: true
  delegate :url, :to => :pic #This is for dynamic styling, everything is now being delegated to Profile::Asset < Image && Blog::Asset < Image


#---------------------------------------------------------------------------------------------------------------#
#------------------------------Original way that works (no dynamic styling)-------------------------------------#
#---------------------------------------------------------------------------------------------------------------#


    # #============================================================##
    # # Paperclip Interpolates
    # #============================================================##
    # Paperclip.interpolates :imageable_type do |attachment, style|
    #   attachment.instance.imageable_type.downcase
    # end

    # Paperclip.interpolates :imageable_id do |attachment, style|
    #   attachment.instance.imageable_id
    # end

    # https://www.viget.com/articles/paperclip-custom-interpolation

    # #============================================================##
    # # Paperclip : Image pic
    # #============================================================##
    # has_attached_file :pic,
    #   :path => "#{Rails.env}/:imageable_type/:imageable_id/:style/:filename",
    #   :styles => { 
    #     :square => ["300x300#", :jpg],
    #     :medium => ["300x190>", :jpg], 
    #     :large => ["825x523>", :jpg]
    #   },
    #   :convert_options => {
    #      # :medium => "-quality 80 -interlace Plane",
    #      # :small => "-quality 80 -interlace Plane",
    #      # :thumb => "-quality 80 -interlace Plane",
    #      # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    #      },
    #   :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }


    # default_url: "/images/:style/missing.png"
    # default_url: "https://placehold.it/600X380"
  

    # validates_attachment :pic, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true


#-------------------------------------------------------------------------------------------------------------------------#


    #----------------------------------------------------------------------------------------#
    #----------------------Failed attempts to dynamic styling images-------------------------#
    #----------------------------------------------------------------------------------------#
        # styles: lambda { |attachment| {
        #     square: (attachment.instance.imageable_type.eql?("Profile") ? ["300x300#", 'jpg'] : {}),
        #     medium: (attachment.instance.imageable_type.eql?("Blog") ? ["300x190>", 'jpg'] : {}),
        #     large: (attachment.instance.imageable_type.eql?("Blog") ? ["825x523>", 'jpg'] : {})
        #   }
        # }

        # styles: lambda { |attachment| if attachment.instance.imageable_type.eql?("Profile")
        #     { 
        #       square: ["300x300#", :jpg]
        #     }
        #   else
        #     {
        #       :medium => ["300x190>", :jpg], 
        #       :large => ["825x523>", :jpg]
        #     }
        #   end
        # }

        # styles: lambda { |a| a.instance.set_styles }
    #----------------------------------------------------------------------------------------#


end


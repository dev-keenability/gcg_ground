require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GcgGround
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_region => 'us-east-1',
      :s3_credentials => {
        :bucket => 'gcg-ground',
        :access_key_id => ENV["S3_ACCESS_KEY_ID"],
        :secret_access_key => ENV["S3_SECRET_ACCESS_KEY"]
        }
      }


    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.default content_type: "text/html"
    # ActionMailer::Base.default :content_type => "text/html"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address:              "smtp.gmail.com",
      port:                 587,
      domain:               "mail.google.com",
      user_name:            ENV["GMAIL_EMAIL"],
      password:             ENV["GMAIL_PASSWORD"],
      authentication:       :plain,
      enable_starttls_auto: true 
    }

  end
end

Rails.application.routes.draw do

  require 'admin_constraint'
  mount Ckeditor::Engine => '/ckeditor', constraints: AdminConstraint.new
  # resources :blogs
  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  devise_for :users
  root 'home#index'
  # resources :images
  resources :profiles, except: [:index] 

  match '/about'              =>        'home#about',              via: [:get],            :as => 'about'
  match '/join-our-team'              =>        'home#join',              via: [:get],            :as => 'join'
  match '/privacy-policy'              =>        'home#privacy_policy',              via: [:get],            :as => 'privacy_policy'

  match '/services-ramp-handling'              =>        'home#services_ramp_handling',              via: [:get],            :as => 'services_ramp_handling'
  match '/services-aircraft-cleaning'              =>        'home#services_aircraft_cleaning',              via: [:get],            :as => 'services_aircraft_cleaning'
  match '/services-passenger-services'              =>        'home#services_passenger_services',              via: [:get],            :as => 'services_passenger_services'
  match '/services-ground-support'              =>        'home#services_ground_support',              via: [:get],            :as => 'services_ground_support'
  match '/services-janitorial-services'              =>        'home#services_janitorial_services',              via: [:get],            :as => 'services_janitorial_services'
  match '/services-cargo-handling'              =>        'home#services_cargo_handling',              via: [:get],            :as => 'services_cargo_handling'
  match '/services-baggage-services'              =>        'home#services_baggage_services',              via: [:get],            :as => 'services_baggage_services'

  #########################################
  #==news url for blog model
  #########################################
    get '/news', to: 'blogs#index', as: :blogs
    post 'news', to: 'blogs#create'
    get '/news/new', to: 'blogs#new', as: :new_blog
    get 'news/:title_for_slug/edit', to: 'blogs#edit', as: 'edit_blog'
    get 'news/:title_for_slug', to: 'blogs#show', as: 'blog'
    patch 'news/:title_for_slug', to: 'blogs#update'
    put 'news/:title_for_slug', to: 'blogs#update'
    delete 'news/:title_for_slug', to: 'blogs#destroy'

  #########################################
  #==locations url for location model
  #########################################
    # get '/locations', to: 'locations#index', as: :locations
    get 'locations/:title_for_slug', to: 'locations#show', as: 'location'
    post 'locations', to: 'locations#create'
    get '/locations/new', to: 'locations#new', as: :new_location
    get 'locations/:title_for_slug/edit', to: 'locations#edit', as: 'edit_location'
    patch 'locations/:title_for_slug', to: 'locations#update'
    put 'locations/:title_for_slug', to: 'locations#update'
    delete 'locations/:title_for_slug', to: 'locations#destroy'

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end
  
  match "/contacts"              =>        "contacts#create",              via: [:post]
  match "/applicants"            =>        "applicants#create",            via: [:post]

  get '/sitemap.xml', to: redirect("https://s3.amazonaws.com/gcg-ground/sitemaps/sitemap.xml.gz", status: 301)

  match "*path", to: "home#catch_all", via: :all
end

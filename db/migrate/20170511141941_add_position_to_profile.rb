class AddPositionToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :position, :integer
  end
end

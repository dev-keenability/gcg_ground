class RemoveAttachmentFromApplicant < ActiveRecord::Migration[5.0]
  def change
    remove_attachment :applicants, :resume
  end
end

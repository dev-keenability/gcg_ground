class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :title_for_slug
      t.text :description
      t.string :phone
      t.string :fax
      t.string :specific_location
      t.text :iframe
      t.string :address
      t.float :latitude
      t.float :longitude
      t.string :title_for_maps

      t.timestamps

    end
  end

end

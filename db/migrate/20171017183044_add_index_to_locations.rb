class AddIndexToLocations < ActiveRecord::Migration[5.0]
  def change
    add_index(:locations, [:title_for_slug], {:unique => true})
  end
end

class AddLocationIdToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :location_id, :integer
  end
end

class AddAirportcodeToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :airportcode, :string
  end
end

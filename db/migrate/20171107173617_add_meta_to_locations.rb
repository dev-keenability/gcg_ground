class AddMetaToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :meta_title, :string
    add_column :locations, :meta_description, :text
  end
end

PGDMP     
                	    u            daqjgrr9ljppvu    9.6.1    9.6.0 L    4           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            5           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            6           1262    3971282    daqjgrr9ljppvu    DATABASE     �   CREATE DATABASE "daqjgrr9ljppvu" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
     DROP DATABASE "daqjgrr9ljppvu";
             rbiqryzefkjzll    false                        2615    2200    public    SCHEMA        CREATE SCHEMA "public";
    DROP SCHEMA "public";
             rbiqryzefkjzll    false            7           0    0    SCHEMA "public"    COMMENT     8   COMMENT ON SCHEMA "public" IS 'standard public schema';
                  rbiqryzefkjzll    false    7                        3079    13277    plpgsql 	   EXTENSION     C   CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
    DROP EXTENSION "plpgsql";
                  false            8           0    0    EXTENSION "plpgsql"    COMMENT     B   COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
                       false    1            �            1259    5186506 
   applicants    TABLE       CREATE TABLE "applicants" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 "   DROP TABLE "public"."applicants";
       public         rbiqryzefkjzll    false    7            �            1259    5186504    applicants_id_seq    SEQUENCE     u   CREATE SEQUENCE "applicants_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE "public"."applicants_id_seq";
       public       rbiqryzefkjzll    false    7    200            9           0    0    applicants_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "applicants_id_seq" OWNED BY "applicants"."id";
            public       rbiqryzefkjzll    false    199            �            1259    3971300    ar_internal_metadata    TABLE     �   CREATE TABLE "ar_internal_metadata" (
    "key" character varying NOT NULL,
    "value" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 ,   DROP TABLE "public"."ar_internal_metadata";
       public         rbiqryzefkjzll    false    7            �            1259    3971326    blogs    TABLE     �  CREATE TABLE "blogs" (
    "id" integer NOT NULL,
    "title" character varying,
    "content" "text",
    "content_index" "text",
    "title_for_slug" character varying,
    "main_image" character varying,
    "pubdate" timestamp without time zone,
    "meta_description" character varying,
    "meta_keywords" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."blogs";
       public         rbiqryzefkjzll    false    7            �            1259    3971324    blogs_id_seq    SEQUENCE     p   CREATE SEQUENCE "blogs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."blogs_id_seq";
       public       rbiqryzefkjzll    false    190    7            :           0    0    blogs_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "blogs_id_seq" OWNED BY "blogs"."id";
            public       rbiqryzefkjzll    false    189            �            1259    3971338    ckeditor_assets    TABLE     �  CREATE TABLE "ckeditor_assets" (
    "id" integer NOT NULL,
    "data_file_name" character varying NOT NULL,
    "data_content_type" character varying,
    "data_file_size" integer,
    "assetable_id" integer,
    "assetable_type" character varying(30),
    "type" character varying(30),
    "width" integer,
    "height" integer,
    "created_at" timestamp without time zone,
    "updated_at" timestamp without time zone
);
 '   DROP TABLE "public"."ckeditor_assets";
       public         rbiqryzefkjzll    false    7            �            1259    3971336    ckeditor_assets_id_seq    SEQUENCE     z   CREATE SEQUENCE "ckeditor_assets_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE "public"."ckeditor_assets_id_seq";
       public       rbiqryzefkjzll    false    192    7            ;           0    0    ckeditor_assets_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE "ckeditor_assets_id_seq" OWNED BY "ckeditor_assets"."id";
            public       rbiqryzefkjzll    false    191            �            1259    5186495    contacts    TABLE       CREATE TABLE "contacts" (
    "id" integer NOT NULL,
    "name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "message" "text",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
     DROP TABLE "public"."contacts";
       public         rbiqryzefkjzll    false    7            �            1259    5186493    contacts_id_seq    SEQUENCE     s   CREATE SEQUENCE "contacts_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."contacts_id_seq";
       public       rbiqryzefkjzll    false    198    7            <           0    0    contacts_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "contacts_id_seq" OWNED BY "contacts"."id";
            public       rbiqryzefkjzll    false    197            �            1259    3971351    images    TABLE     �  CREATE TABLE "images" (
    "id" integer NOT NULL,
    "imageable_id" integer,
    "imageable_type" character varying,
    "name" character varying,
    "pic_file_name" character varying,
    "pic_content_type" character varying,
    "pic_file_size" integer,
    "pic_updated_at" timestamp without time zone,
    "primary" boolean,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."images";
       public         rbiqryzefkjzll    false    7            �            1259    3971349    images_id_seq    SEQUENCE     q   CREATE SEQUENCE "images_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE "public"."images_id_seq";
       public       rbiqryzefkjzll    false    7    194            =           0    0    images_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE "images_id_seq" OWNED BY "images"."id";
            public       rbiqryzefkjzll    false    193            �            1259    7352086 	   locations    TABLE     �  CREATE TABLE "locations" (
    "id" integer NOT NULL,
    "name" character varying,
    "title_for_slug" character varying,
    "description" "text",
    "phone" character varying,
    "fax" character varying,
    "specific_location" character varying,
    "iframe" "text",
    "address" character varying,
    "latitude" double precision,
    "longitude" double precision,
    "title_for_maps" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "navname" character varying,
    "airportcode" character varying,
    "general_contact_email" character varying
);
 !   DROP TABLE "public"."locations";
       public         rbiqryzefkjzll    false    7            �            1259    7352084    locations_id_seq    SEQUENCE     t   CREATE SEQUENCE "locations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE "public"."locations_id_seq";
       public       rbiqryzefkjzll    false    202    7            >           0    0    locations_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE "locations_id_seq" OWNED BY "locations"."id";
            public       rbiqryzefkjzll    false    201            �            1259    3971363    profiles    TABLE     �  CREATE TABLE "profiles" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "title" character varying,
    "telephone" character varying,
    "mobile" character varying,
    "email" character varying,
    "location" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "position" integer,
    "location_id" integer
);
     DROP TABLE "public"."profiles";
       public         rbiqryzefkjzll    false    7            �            1259    3971361    profiles_id_seq    SEQUENCE     s   CREATE SEQUENCE "profiles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."profiles_id_seq";
       public       rbiqryzefkjzll    false    7    196            ?           0    0    profiles_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "profiles_id_seq" OWNED BY "profiles"."id";
            public       rbiqryzefkjzll    false    195            �            1259    3971292    schema_migrations    TABLE     O   CREATE TABLE "schema_migrations" (
    "version" character varying NOT NULL
);
 )   DROP TABLE "public"."schema_migrations";
       public         rbiqryzefkjzll    false    7            �            1259    3971310    users    TABLE     �  CREATE TABLE "users" (
    "id" integer NOT NULL,
    "email" character varying DEFAULT ''::character varying NOT NULL,
    "encrypted_password" character varying DEFAULT ''::character varying NOT NULL,
    "reset_password_token" character varying,
    "reset_password_sent_at" timestamp without time zone,
    "remember_created_at" timestamp without time zone,
    "sign_in_count" integer DEFAULT 0 NOT NULL,
    "current_sign_in_at" timestamp without time zone,
    "last_sign_in_at" timestamp without time zone,
    "current_sign_in_ip" "inet",
    "last_sign_in_ip" "inet",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "admin" boolean
);
    DROP TABLE "public"."users";
       public         rbiqryzefkjzll    false    7            �            1259    3971308    users_id_seq    SEQUENCE     p   CREATE SEQUENCE "users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."users_id_seq";
       public       rbiqryzefkjzll    false    188    7            @           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "users_id_seq" OWNED BY "users"."id";
            public       rbiqryzefkjzll    false    187            �           2604    5186509    applicants id    DEFAULT     j   ALTER TABLE ONLY "applicants" ALTER COLUMN "id" SET DEFAULT "nextval"('"applicants_id_seq"'::"regclass");
 B   ALTER TABLE "public"."applicants" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    199    200    200            �           2604    3971329    blogs id    DEFAULT     `   ALTER TABLE ONLY "blogs" ALTER COLUMN "id" SET DEFAULT "nextval"('"blogs_id_seq"'::"regclass");
 =   ALTER TABLE "public"."blogs" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    190    189    190            �           2604    3971341    ckeditor_assets id    DEFAULT     t   ALTER TABLE ONLY "ckeditor_assets" ALTER COLUMN "id" SET DEFAULT "nextval"('"ckeditor_assets_id_seq"'::"regclass");
 G   ALTER TABLE "public"."ckeditor_assets" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    192    191    192            �           2604    5186498    contacts id    DEFAULT     f   ALTER TABLE ONLY "contacts" ALTER COLUMN "id" SET DEFAULT "nextval"('"contacts_id_seq"'::"regclass");
 @   ALTER TABLE "public"."contacts" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    198    197    198            �           2604    3971354 	   images id    DEFAULT     b   ALTER TABLE ONLY "images" ALTER COLUMN "id" SET DEFAULT "nextval"('"images_id_seq"'::"regclass");
 >   ALTER TABLE "public"."images" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    194    193    194            �           2604    7352089    locations id    DEFAULT     h   ALTER TABLE ONLY "locations" ALTER COLUMN "id" SET DEFAULT "nextval"('"locations_id_seq"'::"regclass");
 A   ALTER TABLE "public"."locations" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    201    202    202            �           2604    3971366    profiles id    DEFAULT     f   ALTER TABLE ONLY "profiles" ALTER COLUMN "id" SET DEFAULT "nextval"('"profiles_id_seq"'::"regclass");
 @   ALTER TABLE "public"."profiles" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    195    196    196            �           2604    3971313    users id    DEFAULT     `   ALTER TABLE ONLY "users" ALTER COLUMN "id" SET DEFAULT "nextval"('"users_id_seq"'::"regclass");
 =   ALTER TABLE "public"."users" ALTER COLUMN "id" DROP DEFAULT;
       public       rbiqryzefkjzll    false    187    188    188            /          0    5186506 
   applicants 
   TABLE DATA               g   COPY "applicants" ("id", "full_name", "email", "phone_number", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    200            A           0    0    applicants_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"applicants_id_seq"', 4, true);
            public       rbiqryzefkjzll    false    199            !          0    3971300    ar_internal_metadata 
   TABLE DATA               U   COPY "ar_internal_metadata" ("key", "value", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    186            %          0    3971326    blogs 
   TABLE DATA               �   COPY "blogs" ("id", "title", "content", "content_index", "title_for_slug", "main_image", "pubdate", "meta_description", "meta_keywords", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    190            B           0    0    blogs_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"blogs_id_seq"', 5, true);
            public       rbiqryzefkjzll    false    189            '          0    3971338    ckeditor_assets 
   TABLE DATA               �   COPY "ckeditor_assets" ("id", "data_file_name", "data_content_type", "data_file_size", "assetable_id", "assetable_type", "type", "width", "height", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    192            C           0    0    ckeditor_assets_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('"ckeditor_assets_id_seq"', 3, true);
            public       rbiqryzefkjzll    false    191            -          0    5186495    contacts 
   TABLE DATA               k   COPY "contacts" ("id", "name", "email", "phone_number", "message", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    198            D           0    0    contacts_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('"contacts_id_seq"', 4, true);
            public       rbiqryzefkjzll    false    197            )          0    3971351    images 
   TABLE DATA               �   COPY "images" ("id", "imageable_id", "imageable_type", "name", "pic_file_name", "pic_content_type", "pic_file_size", "pic_updated_at", "primary", "created_at", "updated_at") FROM stdin;
    public       rbiqryzefkjzll    false    194            E           0    0    images_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('"images_id_seq"', 30, true);
            public       rbiqryzefkjzll    false    193            1          0    7352086 	   locations 
   TABLE DATA                 COPY "locations" ("id", "name", "title_for_slug", "description", "phone", "fax", "specific_location", "iframe", "address", "latitude", "longitude", "title_for_maps", "created_at", "updated_at", "navname", "airportcode", "general_contact_email") FROM stdin;
    public       rbiqryzefkjzll    false    202            F           0    0    locations_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"locations_id_seq"', 4, true);
            public       rbiqryzefkjzll    false    201            +          0    3971363    profiles 
   TABLE DATA               �   COPY "profiles" ("id", "full_name", "title", "telephone", "mobile", "email", "location", "created_at", "updated_at", "position", "location_id") FROM stdin;
    public       rbiqryzefkjzll    false    196            G           0    0    profiles_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"profiles_id_seq"', 13, true);
            public       rbiqryzefkjzll    false    195                       0    3971292    schema_migrations 
   TABLE DATA               1   COPY "schema_migrations" ("version") FROM stdin;
    public       rbiqryzefkjzll    false    185            #          0    3971310    users 
   TABLE DATA                 COPY "users" ("id", "email", "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at", "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip", "created_at", "updated_at", "admin") FROM stdin;
    public       rbiqryzefkjzll    false    188            H           0    0    users_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"users_id_seq"', 1, true);
            public       rbiqryzefkjzll    false    187            �           2606    5186514    applicants applicants_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "applicants"
    ADD CONSTRAINT "applicants_pkey" PRIMARY KEY ("id");
 J   ALTER TABLE ONLY "public"."applicants" DROP CONSTRAINT "applicants_pkey";
       public         rbiqryzefkjzll    false    200    200            �           2606    3971307 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ar_internal_metadata"
    ADD CONSTRAINT "ar_internal_metadata_pkey" PRIMARY KEY ("key");
 ^   ALTER TABLE ONLY "public"."ar_internal_metadata" DROP CONSTRAINT "ar_internal_metadata_pkey";
       public         rbiqryzefkjzll    false    186    186            �           2606    3971334    blogs blogs_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "blogs"
    ADD CONSTRAINT "blogs_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."blogs" DROP CONSTRAINT "blogs_pkey";
       public         rbiqryzefkjzll    false    190    190            �           2606    3971346 $   ckeditor_assets ckeditor_assets_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "ckeditor_assets"
    ADD CONSTRAINT "ckeditor_assets_pkey" PRIMARY KEY ("id");
 T   ALTER TABLE ONLY "public"."ckeditor_assets" DROP CONSTRAINT "ckeditor_assets_pkey";
       public         rbiqryzefkjzll    false    192    192            �           2606    5186503    contacts contacts_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "contacts"
    ADD CONSTRAINT "contacts_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."contacts" DROP CONSTRAINT "contacts_pkey";
       public         rbiqryzefkjzll    false    198    198            �           2606    3971359    images images_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY "images"
    ADD CONSTRAINT "images_pkey" PRIMARY KEY ("id");
 B   ALTER TABLE ONLY "public"."images" DROP CONSTRAINT "images_pkey";
       public         rbiqryzefkjzll    false    194    194            �           2606    7352094    locations locations_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY "locations"
    ADD CONSTRAINT "locations_pkey" PRIMARY KEY ("id");
 H   ALTER TABLE ONLY "public"."locations" DROP CONSTRAINT "locations_pkey";
       public         rbiqryzefkjzll    false    202    202            �           2606    3971371    profiles profiles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "profiles"
    ADD CONSTRAINT "profiles_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."profiles" DROP CONSTRAINT "profiles_pkey";
       public         rbiqryzefkjzll    false    196    196            �           2606    3971299 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY "schema_migrations"
    ADD CONSTRAINT "schema_migrations_pkey" PRIMARY KEY ("version");
 X   ALTER TABLE ONLY "public"."schema_migrations" DROP CONSTRAINT "schema_migrations_pkey";
       public         rbiqryzefkjzll    false    185    185            �           2606    3971321    users users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."users" DROP CONSTRAINT "users_pkey";
       public         rbiqryzefkjzll    false    188    188            �           1259    3971348    idx_ckeditor_assetable    INDEX     m   CREATE INDEX "idx_ckeditor_assetable" ON "ckeditor_assets" USING "btree" ("assetable_type", "assetable_id");
 .   DROP INDEX "public"."idx_ckeditor_assetable";
       public         rbiqryzefkjzll    false    192    192            �           1259    3971347    idx_ckeditor_assetable_type    INDEX     z   CREATE INDEX "idx_ckeditor_assetable_type" ON "ckeditor_assets" USING "btree" ("assetable_type", "type", "assetable_id");
 3   DROP INDEX "public"."idx_ckeditor_assetable_type";
       public         rbiqryzefkjzll    false    192    192    192            �           1259    3971335    index_blogs_on_title_for_slug    INDEX     a   CREATE UNIQUE INDEX "index_blogs_on_title_for_slug" ON "blogs" USING "btree" ("title_for_slug");
 5   DROP INDEX "public"."index_blogs_on_title_for_slug";
       public         rbiqryzefkjzll    false    190            �           1259    3971360 /   index_images_on_imageable_id_and_imageable_type    INDEX     }   CREATE INDEX "index_images_on_imageable_id_and_imageable_type" ON "images" USING "btree" ("imageable_id", "imageable_type");
 G   DROP INDEX "public"."index_images_on_imageable_id_and_imageable_type";
       public         rbiqryzefkjzll    false    194    194            �           1259    7352095 !   index_locations_on_title_for_slug    INDEX     i   CREATE UNIQUE INDEX "index_locations_on_title_for_slug" ON "locations" USING "btree" ("title_for_slug");
 9   DROP INDEX "public"."index_locations_on_title_for_slug";
       public         rbiqryzefkjzll    false    202            �           1259    3971322    index_users_on_email    INDEX     O   CREATE UNIQUE INDEX "index_users_on_email" ON "users" USING "btree" ("email");
 ,   DROP INDEX "public"."index_users_on_email";
       public         rbiqryzefkjzll    false    188            �           1259    3971323 #   index_users_on_reset_password_token    INDEX     m   CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" USING "btree" ("reset_password_token");
 ;   DROP INDEX "public"."index_users_on_reset_password_token";
       public         rbiqryzefkjzll    false    188            /   �   x�}�M�0��u{
.@����C�tS�`C-���Ѩd�d�o���4����{�\b��.D*�"���ƥA�_(�6$��/�=6��3�.v����%5��j����o��p�	�B�-��_琺��.��ش�񷭅RH���ZCn�`X	���!zb��'��c       !   A   x�K�+�,���M�+�,(�O)M.����4204�50�54T02�22�2��3�4537�#����� ܺ      %   m  x��VQo�6~v~���D�eŎ���H��Mк-䅖h�	-j$e�{��؀���쎖�4N��hW`k�y������l�ގ���0pȍ��Bi8Z���k�"��M9�z&2ɲ���������*�des��ͯ�zC.-�1gsH��	�,��	���6�L�:���|�\�g�PN���1���w��� WR�e��|��Yµ5�b&!��Y�5l˄N�7���	���h�okgdu�ezؖ.�C�nߎ�U�C���T�13�@��"c��0����)[�B��b��bNe������;T-4+��zO$� �X:%��6�%��V��t1F�X���BJ*�*,Lp_��u��a�-&��,�N@H��%[�v�)�Y�\
�"B���;�N��`+4G[����J�]��%`J��!Z��rD3Uڦ�,��8�	���P���0.�=Th��խ}`R��sF�k�"��Z,\�X��\%���+�U��@��Rl��;0Ղg�\�8|:���"����{GQo�~��U�H��U}su�Lg$��Z_�����`���SH�\�ٛÓ� �^��A0����	��k�A�d2���C=�6�AY�~�Jς���ӁG�v�T�p?�I}�L����J��N��d���sd����'8U�%�N��?�y�\_�u{�\_����A��g��5�����S�8�'h�"�o������4���<��-i<Q=0�_֚�p�k����F���j7�M��(��������^�~uڝ�VT;fs���!��8�I��&>=h�s�cj̮o����_�3H��\륇Jځ�*�i�K�T��aͣGw&I�q��ۅ3V8���Q��l=P�\�%'�%Y��7lI�)��m�������0���ؐ)��*��R���U�����O4�RɦՌ[Vī�F�����\U����`�Pٹ�̰���$�G�����D���R�RK�����r�I��6YgNg�|$�wS��I�jG|N ���'�8�*]��D�dh,�N}x��n�L�Xm4���.L���)$N�TY�|�=��t˗+qPZ �q���X���!wS�o@��{]�$��&C��^7_�w
��C/���g]��H����bu-xIu-x1�'m�ٷP���n�n5���Ps��m�Q��ժs{HϔR��K�C|v�hu��-�s|H��^��'N�Ui"���r�>�Y<�f�
�vN���ė�@�Ja ��� �/���Kɟ�K�ش�N+��C$�A�i�g'�~54�
M�>4��޿�&��D_����ٻ5��s�����C���E���<ożL�x�p��n5l�v��]��n��Ν�V����^#�n��[[[".R      '   �   x�}�1�0���9h�sb'��X�XDU�U[�Oa�"�x�%z خo��������je�:�e�ݵ\��>�o��c(v���a�X�T16�R�Q%1�$3�%�>"�
�����_q٫\��J2����*��E�B��b�z9�3��5�<B�Rv      -   �   x�}�1n�0Eg�����,[S�3va#�1�ą$���[��� 	���:p��3Υ������gzơ\�~:�~*�jek� ���J�b��V��?���^ccl뺾��?E�0p��%<G�N3$��p� eٻS��2C��'� C�2�͉<�s�{kJW�]Rb��Jl�Z��DSN��rX:�r�j�-r��l;ԝ�@�����w\y�!S�p�xԲ�{���R����      )   �  x���Mo�6@�֯ȱ=�IQ�|�Y�A�M�hn���xg�:���ݴ��t���cj�E��<�DR�\�ɮ��ɧ������#d��}R?��껗OA��	Z���bN>���$0�xE2�6`�@��,����!=�S�B`�~�� �yY���(��cW\��,"��d���ߥBr�,
`tW�g�X3�2a�<8f�^�P�/����J�'�E\�S!��Zh�.c��gb�&�|��uS�1��s[��̎��a�"�k��"�@�YrK�(w1�C	"7���v�@6��q�>��ę҆�d �0��xYwi ���@���E���U=�q����p�+=%mq_��U?�WZ��-�0Ġ����9�,@���Pa �d�%��	��<��*��B�ȀO�T��ף2;)z��yuIm�)g����=gQm�@B�k߾�-n��K�X�W�����_�CVܕ���h�a�������������2 �L�~-�C�4U�Ӯx�p*���$Qp���9��Y���9ѭ\&�@Ԗ9�7������.)�hu1��x�0]wJI3�a�M��j�\���r��,�e�oM�4vCn}F:�)�D&�tB� h��ɮ��vŮ���jOݰPЉ��;sZ��G��՞ԩ���nfc�C��գ�f�	`�7&�x�5��h�g*2^Y� '0�a~�Z��o���i��)a�oB0�v��q�ΜR! �dt��l��T7u�ҟ�>z��d|E�A��1�Q��7��.��C�P�����K������A'����	F�R�.��� g"S�@5�k�?�v?��vut#���.���V�x⢅��H�>�,�;�*2��]�<u���w�7���Ň�1t	��*�.1+��F}����"CV����p:��rX���eq���������^�d>eƘ�l^�c      1     x���r�6ǯ駀����H��o@�dױ��;�y{���$4����U_���O�$e�N�&���K& �����$�[W�B����T��&y?�L:^��F�}����5�Mk2-z������ԥݡ����6�kjS�S�7�oL��k�:wac�|���߸L]�ʵ�@��l���-rm@p�D+�Mh!��t��ژz�ߟ	�7���%�Kgk��6��r[��`o`�e�3o_ݼҚ�B����6LP�²`�ie=,��^C��j���4�K���ۏpkv!�(�fQ�&��5�+�\���ТS���N)�&��|�qע}K�I��x�Qۀ��ߣ"�lxh��l�)�]@2ˆk}�u�n���u`�~hm�U덫c�����|��-K9�s�[L�1�����m��&J���dʸ��\0�|FM��n�!?P����u�n������6]5ͪ�)�vR�M8����,��p�U�!�CG� R
�fXk��LJBG��J�J�QJK̥ #Z`�j���z2LȈD�lD�K��na#�P#��4���ű�}P�X0��.���>����tnə��X="����?~��x�����`k��#v��f�H��1�uE�~v����1Z[�Z��V�E��g�(���>;�{f�aʲ�]n�2�����ӓ>��?ks��0���*N�.�R���?T?$�r��)V�e���^Q"ȣ!��(O������w�ۋw'Wo�|�G,��1כ�@B�k��E�i۵��p��v����;�yW���c�nn|s�zf�,u���,��bW�q|����Z��񮰱6[Bm�x����t���"�M�*�fW�w`��O���*�{��mٚl�7λ��c[�3Xk�w=:>b]�^FT��:�ƅ��,�*� ��j�h@E��P�˥��Rts[k�
�/C�lJ�?>��M��f��-���3=tC��A~x�R*`��_��CP�S&��k�)�'g��`[�G����+؏�Pbޤ��[ �U��5�	):��j)�4$Òj�#pTJ)`I��+ɔ�_�<�xa0�/�큣ʹ���,����.��/�q���0^(�?�1_�-]�D&29�D�)!��S�/2�0��R,�b췆�=�����udM���A/��q�T}�tav�RVsPhn" �c���SW���R{]��)4A�qR�1UK�Jt�
N�1ǜ���=�VJQ	@�JЯ�T�l�dɊq�B-�� m�s��;��zJd���X�{��>	��Zo����2f�2:P:���#��֩�����3.fT�w���ś(��8yU6 mL�]�ɲo��z��5�Ď��E��aN�ކd�[�����v�E֮!���Tؗ@�2@����b�t�B�B���ZYl]�c	^b����?=��5�6BiP�W��߀J��7y\Ķn�ZC:ܩ6��6o!c��A��[P���{9a�ˡ:�-t�V!��#���^��Nr�Y���w`6{�iQ��+p�y��e�\�H��H-�H����j
�'?lr�%��^V���H��0���)z���A?F�3p}�^]"�&ݦ�H�9������O��R�N8�Z0�(�����'������D��g&35=N�B/%YZ]�="*�����?!�'�l���,w�MS�ƫ�q�_ŋ�z�1Q�����'v�׷�!݋&� Ub���R�I�������8��hR��!�g��2��CA��W��'W��9T�M������)
�NԨ�cn����y�B      +   �  x����N�0���Sx	�X�_�*�M��EL�E��J��R&FN���Y�d=:�3RYt������04o�]�g�����~2>��&��	ُ1�ڳF�c�x)����_c��̔Ԕ\af*e+*�5LI�x�h�,f�R����7�-b�����ԏa�ʙ���pZ	C�F�m��D����@~�y|�|ݢ����oc� �[UJ#����E�1TH.7��qg����n���0�	�⣘C=�G�㲴L0��sxOw����Ej��ڏ!��N��]���Xi�t�:�!r2I!
]R�C�+��I��Nr�)�W�E(	RE�љo���G�з��C��u�����1�T�M)���6-����%u��J	p�8-��H0�e����0�,�V������i6*Ɖ0\�TI���9ɕ�E�~hC���r��uZ�����F�S�\�������p����[t���Џ�(Yr�����;�R��_ø��	��	K@l���9�V���z�W}�3�sD�i�"�B��)~M�����Oe?���e�bP�n�_OG�3F5X*z�{�Į�h��x��� ˊsKA�esA�aRk$& GCP�jߡ�&-<�
���c��C�@��t��"J��IK�c�+!�����I�@�K�$@�1���B\��k|{������?��u�Z�����JJi�J��O��"EQ�쵬�          f   x�M���0��S�����?ǵ�K���d,^0�i%�v�b�d�^ыΖ�$�ј�$��6�Z;���lU|�d{c s�N��1��J����c�/4B-�      #   �   x�u�M�@E��p��q��sf\�ARPaT�1�L���}"A���r8�9�Q��uz��v4�V��`�X9���]����;w�^��yvo���	�G��u8N^���٭�M�O@�@��l�&`�܀�%S��Eb@�O=D���H]�rD��C��!{�J����<��Ɓ���f8�     